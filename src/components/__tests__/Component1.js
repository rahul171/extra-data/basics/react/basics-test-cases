import { shallow } from 'enzyme';

// it() and test() functions are alias of each other, there is no difference between them.
// https://stackoverflow.com/a/47399770/5958315

describe('yo', () => {
    it('should do anything man', () => {
        // console.log('it is first');
        expect(1).toBe(1);
    })
});

describe('yo1', () => {
    it('should do anything man1', () => {
        // console.log('it is first1');
        expect(1).toBe(1);
    })
});

describe('yo2', () => {
    it('should do anything man2', () => {
        // console.log('it is first2');
        expect(2).toBe(2);
    })
});

describe('yo3', () => {
    test('something1', () => {
        // console.log('it is first2');
        expect('something').toBe('something');
    })
});

test('something', () => {
    expect(1).toBe(5-4);
})
