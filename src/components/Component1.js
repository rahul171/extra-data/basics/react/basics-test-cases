import React from 'react';

class Component1 extends React.Component {
    render() {
        return (
            <div className="component1">
                <div className="abcd1">
                    hello there
                </div>
                <div className="abcd1">
                    again
                </div>
            </div>
        );
    }
}

export default Component1;
