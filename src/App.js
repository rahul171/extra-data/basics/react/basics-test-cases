import React from 'react';
import './App.css';
import Component1 from './components/Component1';

class App extends React.Component {
    render() {
        return (
            <div className="app">
                <Component1 />
            </div>
        );
    }
}

export default App;
